import React from 'react';
import {BrowserRouter as Router,Route, Switch,Redirect} from 'react-router-dom';
import HomePage from './components/HomePage';
import AboutUsPage from './components/AboutUsPage';
import './App.css';

function App() {
  return (
    <div className="App">
      <Router>
        <Switch>
          <Redirect exact from="/" to="/home"/>
          <Route exact path="/home" component={HomePage}/>
          <Route exact path="/about-us" component={AboutUsPage}/>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
