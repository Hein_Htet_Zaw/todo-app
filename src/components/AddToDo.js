import React, { useEffect,useRef } from 'react'
import { connect } from 'react-redux'
import { addTodo } from '../actions'

const AddTodo = ({ dispatch }) => {
  let input = useRef(null);

  useEffect(()=>{input.current.focus()},[])

  const handleSubmit = (e)=>{
    e.preventDefault()
    if (!input.current.value.trim()) {
      return
    }
    dispatch(addTodo(input.current.value))
    input.current.value = ''
  }

  return (
    <div>
      <form onSubmit={handleSubmit}>
        <input style={{outline: 'none',border: 0,fontSize: '1rem'}} placeholder="Add something to do" ref={input} />
        <button
        type="submit"
        style={{
          backgroundColor: '#1E88E5',
          color: 'white',
          padding: '0.5rem',
          border: 0,
          borderRadius: '4px'}}>
          Add Todo
        </button>
      </form>
    </div>
  )
}

export default connect()(AddTodo)
