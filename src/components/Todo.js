import React from 'react'
import { connect } from 'react-redux'
import {deleteTodo} from '../actions'

const Todo = ({ deleteTodo, onChange, completed, text, id }) => {
  const handleDelete = () => {
    deleteTodo(id)
  }
  return(
    <>
      <li
        style={{
          display:'flex',
          height: '1.5rem'
        }}
      >
        <input type='checkbox' value={!!completed} onChange={onChange}/>
        <p
          style={{
            textAlign: 'center',
            margin: 0,marginLeft: '0.8rem',
            textDecoration: completed ? 'line-through' : 'none'
          }}>
            {text}
        </p>
        <button onClick={handleDelete} style={{outline: 'none',borderRadius: '4px',marginLeft: 'auto',backgroundColor: '#f44336', color: 'white'}}>Delete</button>
      </li>
      <hr/>
    </>
  )
}

const mapDispatchToProps = dispatch => ({
  deleteTodo: id => dispatch(deleteTodo(id))
})

export default connect(null,mapDispatchToProps)(Todo)
