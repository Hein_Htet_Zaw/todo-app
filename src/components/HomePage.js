import React from 'react';
import AddTodo from './AddToDo';
import TodoList from './TodoList';

function HomePage(){
  return(
    <div className='home'>
      <AddTodo />
      <TodoList />
    </div>
  )
}

export default HomePage;